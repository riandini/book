package com.example.book.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.example.book.models.Book;

@Repository
public interface BookRepository extends JpaRepository<Book, Long>{

}
