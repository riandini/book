package com.example.book.controllers;

import java.util.ArrayList;
import java.util.HashMap;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.exceptions.ResourceNotFoundException;
import com.example.book.models.Rating;
import com.example.book.models.dto.AuthorDTO;
import com.example.book.models.dto.RatingDTO;
import com.example.book.repositories.RatingRepository;

@RestController
@RequestMapping("/api")
public class RatingController {
	
	@Autowired
	RatingRepository ratingRepository;
	
	// Get All Rating DTO Mapper
	@GetMapping("/rating/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Rating dengan menggunakan method findAll() dari repository
		ArrayList<Rating> listRatingEntity = (ArrayList<Rating>) ratingRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<RatingDTO> listRatingDTO = new ArrayList<RatingDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Rating rating : listRatingEntity) {

			//inisialsasi object paper DTO
			RatingDTO ratingDTO = modelMapper.map(rating, RatingDTO.class);
			
			//memasukan object author DTO ke arraylist
			listRatingDTO.add(ratingDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Rating Data Success");
		result.put("Data", listRatingDTO);
						
		return result;
	}
	// Create a new Rating DTO Mapper
	@PostMapping("/rating/create")
	public HashMap<String, Object> createRatingDTOMapper(@Valid @RequestBody RatingDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Rating ratingEntity = modelMapper.map(body, Rating.class);
		
		//ini proses save data ke database
		ratingRepository.save(ratingEntity);				
		body.setRatingID(ratingEntity.getRatingID());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Rating Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Update Rating DTO Mapper
	@PutMapping("/rating/update/{id}")
	public HashMap<String, Object> updateRatingDTOMapper(@PathVariable(value = "id") Long ratingID, 
			@Valid @RequestBody RatingDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Rating ratingEntity = ratingRepository.findById(ratingID)
	            .orElseThrow(() -> new ResourceNotFoundException("Rating", "id", ratingID));
		
		ModelMapper modelMapper = new ModelMapper();
		ratingEntity = modelMapper.map(body, Rating.class);
		ratingEntity.setRatingID(ratingID);
		
		//ini proses save data ke database
		ratingRepository.save(ratingEntity);				
		body.setRatingID(ratingEntity.getRatingID());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update New Author Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a Rating DTO Mapper
	@DeleteMapping("/rating/delete/{id}")
	public HashMap<String, Object> deleteRatingDTOMapper(@PathVariable(value = "id") Long ratingID, AuthorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Rating ratingEntity = ratingRepository.findById(ratingID)
	            .orElseThrow(() -> new ResourceNotFoundException("Rating", "id", ratingID));
		
		ModelMapper modelMapper = new ModelMapper();
		ratingEntity = modelMapper.map(body, Rating.class);
		ratingEntity.setRatingID(ratingID);
		
		//ini proses delete data
		ratingRepository.delete(ratingEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Rating Success");
				
		return result;
	}

}
