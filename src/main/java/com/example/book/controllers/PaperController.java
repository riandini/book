package com.example.book.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.exceptions.ResourceNotFoundException;
import com.example.book.models.Paper;
import org.modelmapper.ModelMapper;
import com.example.book.models.dto.PaperDTO;
import com.example.book.repositories.PaperRepository;

@RestController
@RequestMapping("/api")
public class PaperController {
	
	@Autowired
	PaperRepository paperRepository;
	
	// Get All Paper mapper
	@GetMapping("/paper/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari paper dengan menggunakan method findAll() dari repository
		ArrayList<Paper> listPaperEntity = (ArrayList<Paper>) paperRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<PaperDTO> listPaperDTO = new ArrayList<PaperDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Paper paper : listPaperEntity) {

			PaperDTO paperDTO = modelMapper.map(paper,PaperDTO.class);
			
			//memasukan object paper DTO ke arraylist
			listPaperDTO.add(paperDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Paper Data Success");
		result.put("Data", listPaperDTO);
						
		return result;
	}
	
	// Create a new Paper Mapper
	@PostMapping("/paper/create")
	public HashMap<String, Object> createPaperDTO(@Valid @RequestBody PaperDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Paper paper = modelMapper.map(body,Paper.class);
		
		//ini proses save data ke database
		paperRepository.save(paper);	
		body.setPaperId(paper.getPaperId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Paper Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Update a Paper Mapper
	@PutMapping("/paper/update/{id}")
	public HashMap<String, Object> updatePaperMapper(@PathVariable(value = "id") Long paperId, 
			@Valid @RequestBody PaperDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepository.findById(paperId)
	            .orElseThrow(() -> new ResourceNotFoundException("Paper", "id",  paperId));
		
		ModelMapper modelMapper = new ModelMapper();
		paperEntity = modelMapper.map(body,Paper.class);
		paperEntity.setPaperId(paperId);
		
		//ini proses save data ke database
		paperRepository.save(paperEntity);
		body = modelMapper.map(paperEntity, PaperDTO.class);
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Paper Success");
		result.put("Data", body);
		
		return result;
	}
	
	// Delete a Paper DTO Mapper
	@DeleteMapping("/paper/delete/{id}")
	public HashMap<String, Object> deletePaperMapper(@PathVariable(value = "id") Long paperId, PaperDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Paper paperEntity = paperRepository.findById(paperId)
	            .orElseThrow(() -> new ResourceNotFoundException("Paper", "id", paperId));
		
		ModelMapper modelMapper = new ModelMapper();
		paperEntity = modelMapper.map(body,Paper.class);
		paperEntity.setPaperId(paperId);
	
		paperRepository.delete(paperEntity);	
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Paper Success");
		
		return result;
	}
	
	

}
