package com.example.book.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.repositories.AuthorRepository;
import com.example.book.exceptions.ResourceNotFoundException;
import com.example.book.models.Author;
import com.example.book.models.dto.AuthorDTO;

@RestController
@RequestMapping("/api")
public class AuthorController {
	
	@Autowired
	AuthorRepository authorRepository;
	
	// Get All Author DTO Mapper
	@GetMapping("/author/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Author dengan menggunakan method findAll() dari repository
		ArrayList<Author> listAuthorEntity = (ArrayList<Author>) authorRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<AuthorDTO> listAuthorDTO = new ArrayList<AuthorDTO>();
		ModelMapper modelMapper = new ModelMapper();
		for(Author author : listAuthorEntity) {

			//inisialsasi object paper DTO
			AuthorDTO authorDTO = modelMapper.map(author, AuthorDTO.class);
			
			//memasukan object author DTO ke arraylist
			listAuthorDTO.add(authorDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Author Data Success");
		result.put("Data", listAuthorDTO);
						
		return result;
	}
		
	// Create a new Author DTO Mapper
	@PostMapping("/author/create")
	public HashMap<String, Object> createAuthorDTOMapper(@Valid @RequestBody AuthorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Author authorEntity = modelMapper.map(body, Author.class);
		
		//ini proses save data ke database
		authorRepository.save(authorEntity);				
		body.setAuthorId(authorEntity.getAuthorId());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Author Success");
		result.put("Data", body);
				
		return result;
	}
		
	// Update Author DTO Mapper
	@PutMapping("/author/update/{id}")
	public HashMap<String, Object> updateAuthorDTOMapper(@PathVariable(value = "id") Long authorId, 
			@Valid @RequestBody AuthorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepository.findById(authorId)
	            .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
		
		ModelMapper modelMapper = new ModelMapper();
		authorEntity = modelMapper.map(body, Author.class);
		authorEntity.setAuthorId(authorId);
		
		//ini proses save data ke database
		authorRepository.save(authorEntity);				
		body.setAuthorId(authorEntity.getAuthorId());
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Author Success");
		result.put("Data", body);
				
		return result;
	}
	
	// Delete a Author DTO Mapper
	@DeleteMapping("/author/delete/{id}")
	public HashMap<String, Object> deleteAuthorDTOMapper(@PathVariable(value = "id") Long authorId, AuthorDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		Author authorEntity = authorRepository.findById(authorId)
	            .orElseThrow(() -> new ResourceNotFoundException("Author", "id", authorId));
		
		ModelMapper modelMapper = new ModelMapper();
		authorEntity = modelMapper.map(body, Author.class);
		authorEntity.setAuthorId(authorId);
		
		//ini proses delete data
		authorRepository.delete(authorEntity);
				
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Author Success");
				
		return result;
	}

}
