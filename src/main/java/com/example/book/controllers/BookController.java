package com.example.book.controllers;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.repositories.BookRepository;
import com.example.book.exceptions.ResourceNotFoundException;
import com.example.book.models.Author;
import com.example.book.models.Book;
import com.example.book.models.Publisher;
import com.example.book.models.dto.AuthorDTO;
import com.example.book.models.dto.BookDTO;
import com.example.book.models.dto.PaperDTO;
import com.example.book.models.dto.PublisherDTO;

@RestController
@RequestMapping("/api")
public class BookController {
	
	@Autowired
	BookRepository bookRepository;
	
	// Get All Book DTO Mapper
	@GetMapping("book/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari Book dengan menggunakan method findAll() dari repository
		ArrayList<Book> listBookEntity = (ArrayList<Book>) bookRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<BookDTO> listBookDTO = new ArrayList<BookDTO>();
		ModelMapper modelMapper = new ModelMapper();
		
		//mapping semua object dari entity ke DTO menggunakan looping
		for(Book book : listBookEntity) {
			
			BookDTO bookDTO = modelMapper.map(book, BookDTO.class);
					
			//memasukan object book DTO ke arraylist
			listBookDTO.add(bookDTO);
		}
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Book Data Success");
		result.put("Data", listBookDTO);
						
		return result;
	}
	
	// Create a new Book DTO Mapper
	@PostMapping("/book/create")
	public HashMap<String, Object> createBookDTOMapper(@Valid @RequestBody BookDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();

		ModelMapper modelMapper = new ModelMapper();
		Book bookEntity = modelMapper.map(body, Book.class);
		Author authorEntity = modelMapper.map(body, Author.class);
		Publisher publisherEntity = modelMapper.map(body, Publisher.class);
		
		authorEntity.setAuthorId(body.getAuthor().getAuthorId());
		publisherEntity.setPublisherId(body.getPublisher().getPublisherId());
		
		bookEntity.setPrice(calculateBookPrice(body.getPublisher().getPublisherId(), body.getAuthor().getAuthorId()));

		
		bookRepository.save(bookEntity);	
		
		body.setBookId(bookEntity.getBookId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Book Success");
		result.put("Data", body);
		
		return result;
		
	}

	public BigDecimal calculateBookPrice (Long publisherId, Long authorId) {
		BigDecimal paperPrice = new BigDecimal(0);
		BigDecimal ratePrice = new BigDecimal(0);
		BigDecimal rateBookPrice = new BigDecimal(1.1);
		BigDecimal bookPrice = new BigDecimal(0);
		ArrayList<Book> listBookEntity = (ArrayList<Book>) bookRepository.findAll();
		
		
		for (Book book : listBookEntity) {
			if((book.getPublisher().getPublisherId() == publisherId) && (book.getAuthor().getAuthorId() == authorId)) {
				paperPrice = book.getPublisher().getPaperQuality().getPaperPrice();
				ratePrice = book.getAuthor().getRating().getRatePrice();

			}
		}
		bookPrice = paperPrice.multiply(ratePrice).multiply(rateBookPrice);
		
		return bookPrice;
		
	}
	
	
	// Update a Book DTO Mapper
	@PutMapping("/book/update/{id}")
	public HashMap<String, Object> updateBookDTOMapper(@PathVariable(value = "id") Long bookId, 
			@Valid @RequestBody BookDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
	
		Book bookEntity = bookRepository.findById(bookId)
				.orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
		
		ModelMapper modelMapper = new ModelMapper();
		Author authorEntity = modelMapper.map(body, Author.class);
		Publisher publisherEntity = modelMapper.map(body, Publisher.class);
		
		authorEntity.setAuthorId(body.getAuthor().getAuthorId());
		publisherEntity.setPublisherId(body.getPublisher().getPublisherId());
		
		bookEntity = modelMapper.map(body, Book.class);
		bookEntity.setBookId(bookId);
		bookEntity.setPrice(calculateBookPrice(body.getPublisher().getPublisherId(), body.getAuthor().getAuthorId()));
		
		bookRepository.save(bookEntity);
		body.setBookId(bookEntity.getBookId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Book Success");
		result.put("Data", body);
		
		return result;
		
	}
	
	// Delete a Book DTO Mapper
	@DeleteMapping("/book/delete/{id}")
	public HashMap<String, Object> deleteBookDTOMapper(@PathVariable(value = "id") Long bookId, BookDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
	
		Book bookEntity = bookRepository.findById(bookId)
				.orElseThrow(() -> new ResourceNotFoundException("Book", "id", bookId));
		
		ModelMapper modelMapper = new ModelMapper();
		bookEntity = modelMapper.map(body, Book.class);
		bookEntity.setBookId(bookId);
		
		//ini proses delete data
		bookRepository.delete(bookEntity);
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Book Success");
		
		return result;
	}

}
