package com.example.book.controllers;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import javax.validation.Valid;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.book.exceptions.ResourceNotFoundException;
import com.example.book.models.Paper;
import com.example.book.models.Publisher;
import com.example.book.models.dto.PaperDTO;
import com.example.book.models.dto.PublisherDTO;
import com.example.book.repositories.PublisherRepository;

@RestController
@RequestMapping("/api")
public class PublisherController {
	
	@Autowired
	PublisherRepository publisherRepository;
	
	// Get All Publisher Mapper
	@GetMapping("publisher/read")
	public HashMap<String, Object> getAllDataMapper() {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		//Ambil semua data dari publisher dengan menggunakan method findAll() dari repository
		ArrayList<Publisher> listPublisherEntity = (ArrayList<Publisher>) publisherRepository.findAll();
		
		//Buat sebuah arrayList DTO
		ArrayList<PublisherDTO> listPublisherDTO = new ArrayList<PublisherDTO>();
		ModelMapper modelMapper = new ModelMapper();
		//mapping semua object dari entity ke DTO menggunakan looping
		for(Publisher publisher : listPublisherEntity) {

			PublisherDTO publisherDTO = modelMapper.map(publisher, PublisherDTO.class);
			
			listPublisherDTO.add(publisherDTO);
		}
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Read All Publisher Data Success");
		result.put("Data", listPublisherDTO);
				
		return result;
	}
	
	// Create a new Publisher DTO
	@PostMapping("/publisher/create")
	public HashMap<String, Object> createPublisherDTOMapper(@Valid @RequestBody PublisherDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
		ModelMapper modelMapper = new ModelMapper();
		Publisher publisherEntity = modelMapper.map(body,Publisher.class);
		Paper paperEntity = modelMapper.map(body,Paper.class);
		
		paperEntity.setPaperId(body.getPaperQuality().getPaperId());
		
		
		//ini proses save data ke database
		publisherRepository.save(publisherEntity);
		body.setPublisherId(publisherEntity.getPublisherId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Create New Publisher Success");
		result.put("Data", body);
		
		return result;
		
	}
	
	// Update a Publisher DTO Mapper
	@PutMapping("/publisher/update/{id}")
	public HashMap<String, Object> updatePublisherDTOMapper(@PathVariable(value = "id") Long publisherId,
			@Valid @RequestBody PublisherDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
	
		Publisher publisherEntity = publisherRepository.findById(publisherId)
	            .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));
		
		ModelMapper modelMapper = new ModelMapper();
		Paper paperEntity = modelMapper.map(body,Paper.class);
		paperEntity.setPaperId(body.getPaperQuality().getPaperId());
		publisherEntity = modelMapper.map(body,Publisher.class);
		publisherEntity.setPublisherId(publisherId);
	
		//ini proses save data ke database
		publisherRepository.save(publisherEntity);
		body.setPublisherId(publisherEntity.getPublisherId());
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Update Publisher Success");
		result.put("Data", body);
		
		return result;
		
	}
	
	// Delete a Publisher DTO
	@DeleteMapping("/publisher/delete/{id}")
	public HashMap<String, Object> deletePublisherDTOMapper(@PathVariable(value = "id") Long publisherId, PublisherDTO body) {
		HashMap<String, Object> result = new HashMap<String, Object>();
		
	
		Publisher publisherEntity = publisherRepository.findById(publisherId)
	            .orElseThrow(() -> new ResourceNotFoundException("Publisher", "id", publisherId));
		
		ModelMapper modelMapper = new ModelMapper();
		publisherEntity =  modelMapper.map(body, Publisher.class);
		publisherEntity.setPublisherId(publisherId);
		
		//ini proses delete data database
		publisherRepository.delete(publisherEntity);
		
		//Maping result untuk response API
		result.put("Status", 200);
		result.put("Message", "Delete Publisher Success");
		
		return result;
		
	}

}
