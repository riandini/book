package com.example.book.models.dto;

import java.math.BigDecimal;
import java.util.Date;

public class BookDTO {
	
	private Long bookId;
	private String title;
	private Date releaseDate;
	private BigDecimal price;
	private AuthorDTO author;
	private PublisherDTO publisher;
	
	
	public BookDTO() {
		super();
	}
	
	public BookDTO(Long bookId, String title, Date releaseDate, BigDecimal price, AuthorDTO author,
			PublisherDTO publisher) {
		super();
		this.bookId = bookId;
		this.title = title;
		this.releaseDate = releaseDate;
		this.price = price;
		this.author = author;
		this.publisher = publisher;
	}

	public Long getBookId() {
		return bookId;
	}


	public void setBookId(Long bookId) {
		this.bookId = bookId;
	}


	public String getTitle() {
		return title;
	}


	public void setTitle(String title) {
		this.title = title;
	}


	public Date getReleaseDate() {
		return releaseDate;
	}


	public void setReleaseDate(Date releaseDate) {
		this.releaseDate = releaseDate;
	}


	public AuthorDTO getAuthor() {
		return author;
	}


	public void setAuthor(AuthorDTO author) {
		this.author = author;
	}


	public PublisherDTO getPublisher() {
		return publisher;
	}


	public void setPublisher(PublisherDTO publisher) {
		this.publisher = publisher;
	}

	public BigDecimal getPrice() {
		return price;
	}

	public void setPrice(BigDecimal price) {
		this.price = price;
	}
	
	
	
}
