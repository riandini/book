package com.example.book.models;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Positive;

import org.modelmapper.ModelMapper;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import com.example.book.models.dto.PaperDTO;

@Entity
@Table(name = "paperQuality")
@EntityListeners(AuditingEntityListener.class)

public class Paper implements Serializable {
	
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long paperId;
	
	@Column(nullable = false)
	private String qualityName;
	
	@Column(nullable = false)
	private BigDecimal paperPrice;
	
	@OneToMany(
	        mappedBy = "paperQuality",
	        cascade = CascadeType.PERSIST,
	        fetch = FetchType.LAZY
	    )
	private Set<Publisher> publishers;
	
//	ModelMapper modelMapper = new ModelMapper();
//	PaperDTO paperDTO = modelMapper.map(paper,PaperDTO.class);
	
	public Paper() {
		publishers = new HashSet<>();
	}
	
	public Paper(String qualityName, BigDecimal paperPrice) {
		this.qualityName = qualityName;
		this.paperPrice = paperPrice;
		
		publishers = new HashSet<>();
	}



	public Long getPaperId() {
		return paperId;
	}

	public void setPaperId(Long paperId) {
		this.paperId = paperId;
	}

	public String getQualityName() {
		return qualityName;
	}

	public void setQualityName(String qualityName) {
		this.qualityName = qualityName;
	}

	public BigDecimal getPaperPrice() {
		return paperPrice;
	}

	public void setPaperPrice(BigDecimal paperPrice) {
		this.paperPrice = paperPrice;
	}


	public Set<Publisher> getPublishers() {
		return publishers;
	}


	public void setPublishers(Set<Publisher> publishers) {
		this.publishers = publishers;
		for (Publisher publisher : publishers) {
        	publisher.setPaperQuality(this);
        	}
    }
	

}
