package com.example.book.models;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import org.springframework.data.jpa.domain.support.AuditingEntityListener;

@Entity
@Table(name = "publisher")
@EntityListeners(AuditingEntityListener.class)

public class Publisher implements Serializable {
	@Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long publisherId;
	
	@Column(nullable = false)
	private String companyName;
	
	@Column(nullable = false)
	private String country;
	
	@ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "paper_ID")
	private Paper paperQuality;
	
	
	@OneToMany(
	        mappedBy = "publisher",
	        cascade = CascadeType.PERSIST,
	        fetch = FetchType.LAZY
	    )
	private Set<Book> books;
	
	public Publisher() {
		books = new HashSet<>();
	}
	
	public Publisher(String companyName, String country, Paper paperQuality) {
		super();
		this.companyName = companyName;
		this.country = country;
		this.paperQuality = paperQuality;
		books = new HashSet<>();
	}
	

	public Long getPublisherId() {
		return publisherId;
	}


	public void setPublisherId(Long publisherId) {
		this.publisherId = publisherId;
	}


	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}


	public Paper getPaperQuality() {
		return paperQuality;
	}


	public void setPaperQuality(Paper paperQuality) {
		this.paperQuality = paperQuality;
		paperQuality.getPublishers().add(this);
	
	}


	public Set<Book> getBooks() {
		return books;
	}

	public void setBooks(Set<Book> books) {
		this.books = books;
		for (Book book : books) {
            book.setPublisher(this);
        }
	}

}
